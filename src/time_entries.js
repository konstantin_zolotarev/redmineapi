/**
 * Base Time Entries API methods
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @constructor
 * @struct
 * @since 1.1
 * @throws {Error} No redmine.Request passed in.
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_News
 * @param {redmine.Request} request
 * @returns {redmine.TimeEntries}
 */
redmine.TimeEntries= function(request) {
    if (!request || !request instanceof redmine.Request) {
        throw new Error("No redmine.Request passed in.");
    }
    this.request = request;
};

/**
 * Load list of activities for redmine
 *
 * @since 2.2
 * @param callback
 */
redmine.TimeEntries.prototype.getActivities = function(callback) {
    callback = callback || redmine.callback;
    this.request.get("/enumerations/time_entry_activities.json", callback);
}

/**
 * Listing time entries
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries#Listing-time-entries
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.TimeEntries.prototype.all = function(callback) {
	callback = callback || redmine.callback;
	this.request.get("/time_entries.json", callback);
};

/**
 * Returns the time entry of given id
 * 
 * @throw {Error} if no details provided
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries#Showing-a-time-entry
 * @param {(number|string)} id Time entry ID
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.TimeEntries.prototype.get = function(id, callback) {
	if (arguments.length < 2 || !id) {
		throw new Error("Please provide time entry id");
	}
	callback = callback || redmine.callback;
	this.request.get("/time_entries/"+id+".json", callback);
};

/**
 * Creating a time entry
 * 
 * @throw {Error} if no details provided
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries#Creating-a-time-entry
 * @param {Object} timeEntry a hash of the time entry attributes
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.TimeEntries.prototype.create = function(timeEntry, callback) {
	if (arguments.length < 2 || !timeEntry || typeof timeEntry != "object") {
		throw new Error("Please provide time entry details");
	}
	var data = {
		'time_entry': timeEntry
	};
	this.request.post("/time_entries.json", data, callback);
};

/**
 * Updates the time entry of given id
 *
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries#Updating-a-time-entry
 * @throw {Error} if no details provided
 * @param {(number|string)} id Time entry ID
 * @param {Object} timeEntry a hash of the time entry attributes
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.TimeEntries.prototype.update = function(id, timeEntry, callback) {
	if (arguments.length < 3 || !id || !timeEntry || typeof timeEntry != "object") {
		throw new Error("Please provide time entry id and details");
	}
	var data = {
		'time_entry': timeEntry
	};
	this.request.put("/time_entries/"+id+".json", data, callback);
};

/**
 * Deletes the time entry of given id
 *
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries#Deleting-a-time-entry
 * @throw {Error} if no details provided
 * @param {(number|string)} id Time entry ID
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.TimeEntries.prototype.del = function(id, callback) {
	if (arguments.length < 2 || !id) {
		throw new Error("No time entry ID provided.");
	}
	this.request.del("/time_entries/"+id+".json", callback);
};