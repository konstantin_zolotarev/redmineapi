/**
 * Base projects API methods
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @constructor
 * @struct
 * @since 1.0
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Projects
 * @param {redmine.Request} request
 * @returns {redmine.Projects}
 */
redmine.Projects = function(request) {
    if (!request || !request instanceof redmine.Request) {
        throw "No redmine.Request passed in.";
    }
    this.request = request;
};


/**
 * Fetch all projects from Redmine
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Projects#Listing-projects
 * @param {?string} filters list of additional filters. For ex: limit=2&offset=1
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Projects.prototype.all = function(filters, callback) {
    if (arguments.length < 2 && typeof filters == "function") {
        callback = filters;
        filters = "";
    }
    filters = filters || "";
    callback = callback || redmine.callback;
    this.request.get("/projects.json?"+filters, callback);
};

/**
 * Fetching project details
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Projects#Showing-a-project
 * @param {(number|string)} id Project ID or Key
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Projects.prototype.get = function(id, callback) {
    if (!id || arguments.length < 2) {
        throw new Error("No project ID provided");
    }
    callback = callback || redmine.callback;
    this.request.get("/projects/"+id+".json?include=trackers,issue_categories", callback);
};

/**
 * Creates a new Project in redmine 
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Projects#Creating-a-project
 * @throws {Error} error if no name or identifier not passed
 * @param {string} name
 * @param {string} identifier
 * @param {string=} description
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Projects.prototype.create = function(name, identifier, description, callback) {
    //check input data
    if (!name) {
        throw new Error("please provide project name");
    }
    if (!identifier) {
        throw new Error("please provide project identifier");
    }
    //check for description
    if (arguments.length == 3 && typeof description == "function") {
        callback = description;
        description = "";
    }
    callback = callback || redmine.callback;
    var data = {
        'project': {
            'name': name,
            'identifier': identifier,
            'description': description || ""
        }
    };
    this.request.post("/projects.json", JSON.stringify(data), callback);
};

/**
 * Update project details
 *
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Projects#Updating-a-project
 * @throws {Error} error if no ID or project details provided
 * @param {(number|string)} id Project ID or key
 * @param {Object} project Preject details
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Projects.prototype.update = function(id, project, callback) {
    if (arguments.length != 3) {
        throw new Error("Please provide project details");
    }
    if (!id) {
        throw new Error("No ID provided.");
    }
    if (!project || typeof project != "object") {
        throw new Error("No project provided");
    }
    callback = callback || redmine.callback;
    var data = {
        'project': project
    };
    this.request.put("/projects/"+id+".json", JSON.stringify(data), callback);
};

/**
 * Remove project from Redmine
 *
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Projects#Deleting-a-project
 * @throws {Error} Error if no Id passed
 * @param {string} id Project ID or key
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Projects.prototype.del = function(id, callback) {
    if (!id) {
        throw new Error("No ID passed");
    }
    callback = callback || redmine.callback;
    this.request.del("/projects/"+id+".json", callback);
};

/**
 * Returns a paginated list of the project memberships. Project id can be either the project numerical id or the project identifier.
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Memberships#Project-Memberships
 * @since 1.4
 * @throws {Error} Error if no Id passed
 * @param {string} id Project ID or key
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Projects.prototype.memberships = function(id, callback) {
    if (!id) {
        throw new Error("No project ID passed");
    }
    callback = callback || redmine.callback;
    this.request.get("/projects/"+id+"/memberships.json", callback);
};