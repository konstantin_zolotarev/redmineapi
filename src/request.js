'use strict';

/**
 * Base class for redmine requests. 
 * It will extend basic XMLHttpRequest
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @constructor
 * @param {redmine.Config} config Default config for Request
 */
redmine.Request = function(config) {
    if (!config) {
        config = redmine.Config;
    }        
    this.config = redmine.merge(redmine.Config, config);
    /**
     * Check host and remove last slash (/)
     */
    if (this.config.host && this.config.host.lastIndexOf('/') == (this.config.host.length - 1)) {
        this.config.host = this.config.host.substr(0, this.config.host.length - 1);
    }
};

/**
 * Create a XMLHttpRequest object for future use
 * 
 * @private 
 * @param {String} method HTTP Method (GET, POST, ...)
 * @param {String} url URL to make a request
 * @param {Boolean=} async Request will be async ?
 * @param {Boolean=} preventContentType Prevents setting Content-Type header. Will be need for file uploading
 * @returns {XMLHttpRequest} Created XmlHttpRequest object
 */
redmine.Request.prototype.create = function(method, url, async, preventContentType) {
    var xhr = new XMLHttpRequest();
    if (url[0] != '/') {
        url = '/' + url;
    }
    var fullUrl = this.config.host + url;
    if (this.config.useHttpAuth) {
        xhr.open(method, fullUrl, (async || true), this.config.httpUser, this.config.httpPassword);
    } else {
        xhr.open(method, fullUrl, (async || true));
    }
    if (this.config.chiliProject) {
        //We need another header info for connecting to ChiliProject
        xhr.setRequestHeader("X-ChiliProject-API-Key", this.config.apiAccessKey);
    } else {
        xhr.setRequestHeader("X-Redmine-API-Key", this.config.apiAccessKey);
    }
    if (!preventContentType) {
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    }
    return xhr;
};

/**
 * Perform HTTP GET request to Redmine server
 * 
 * @param {String} url
 * @param {redmine.callback=} [callback] Callback for request
 */
redmine.Request.prototype.get = function(url, callback) {
    var xhr = this.create("GET", url);
    //Check input date
    callback = callback || redmine.callback;
    //success handler
    xhr.onload = function(e) {
        if (this.status == 200 || this.status == 201) {
            var data = {};
            if (this.response && this.response != "") {
                try {
                    data = JSON.parse(this.response);
                } catch(e) {}
            }
            callback(null, data);
        } else {
            callback({'error': e, 'request': this}, null);
        }
    };
    //sutup default error handler
    xhr.onerror = function(e) {
        callback({'error': e, 'request': this}, null);
    };
    xhr.send();
};

/**
 * Perform HTTP POST request to Redmine server
 * 
 * @param {String} url
 * @param {*} data Data that would e send to server
 * @param {redmine.callback=} [callback] Callback for request
 */
redmine.Request.prototype.post = function(url, data, callback) {
    var xhr = this.create("POST", url);
    //Check input date
    data = data || "";
    callback = callback || redmine.callback;
    //check data
    if (typeof data != 'string') {
        data = JSON.stringify(data);
    }
    //success handler
    xhr.onload = function(e) {
        if (this.status == 200 || this.status == 201) {
            var data = {};
            if (this.response && this.response != "") {
                try {
                    data = JSON.parse(this.response);
                } catch(e) {}
            }
            callback(null, data);
        } else {
            callback({'error': e, 'request': this}, null);
        }
    };
    //sutup default error handler
    xhr.onerror = function(e) {
        callback({'error': e, 'request': this}, null);
    };
    xhr.send(data);
};

/**
 * Perform HTTP PUT request to Redmine server
 * 
 * @param {String} url
 * @param {*} data Data that would e send to server
 * @param {redmine.callback=} [callback] Callback for request
 */
redmine.Request.prototype.put = function(url, data, callback) {
    var xhr = this.create("PUT", url);
    //Check input date
    data = data || "";
    callback = callback || redmine.callback;
    //success handler
    xhr.onload = function(e) {
        if (this.status == 200 || this.status == 201) {
            var data = {};
            if (this.response && this.response != "") {
                try {
                    data = JSON.parse(this.response);
                } catch(e) {}
            }
            callback(null, data);
        } else {
            callback({'error': e, 'request': this}, null);
        }
    };
    //error handler
    xhr.onerror = function(e) {
        callback({'error': e, 'request': this}, null);
    };
    xhr.send(data);
};

/**
 * Perform HTTP DELETE request to Redmine server
 * 
 * @param {String} url
 * @param {redmine.callback=} [callback] Callback for request
 */
redmine.Request.prototype.del = function(url, callback) {
    var xhr = this.create("DELETE", url);
    //Check input date
    callback = callback || redmine.callback;
    //success handler
    xhr.onload = function(e) {
        if (this.status == 200 || this.status == 201) {
            var data = {};
            if (this.response && this.response != "") {
                try {
                    data = JSON.parse(this.response);
                } catch(e) {}
            }
            callback(null, data);
        } else {
            callback({'error': e, 'request': this}, null);
        }
    };
    //sutup default error handler
    xhr.onerror = function(e) {
        callback({'error': e, 'request': this}, null);
    };
    xhr.send();
};

/**
 * Perform HTTP POST request with Content-Type=application/octet-stream to Redmine server
 * 
 * @param {String} url
 * @param {*} data Data that will be sent to server
 * @param {redmine.callback=} [callback] Callback for request
 */
redmine.Request.prototype.upload = function(url, data, callback) {
    var xhr = this.create("POST", url, true, true);
    xhr.setRequestHeader("Content-Type", "application/octet-stream");
    //Check input date
    data = data || "";
    callback = callback || redmine.callback;
    //success handler
    xhr.onload = function(e) {
        if (this.status == 200 || this.status == 201) {
            var data = JSON.parse(this.response);
            callback(null, data);
        } else {
            callback({'error': e, 'request': this}, null);
        }
    };
    //error handler
    xhr.onerror = function(e) {
        callback({'error': e, 'request': this}, null);
    };
    xhr.send(data);
};