/**
 * Base Users API methods
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @constructor
 * @struct
 * @since 1.1
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Users
 * @param {redmine.Request} request
 * @returns {redmine.Users}
 */
redmine.Users = function(request) {
    if (!request || !request instanceof redmine.Request) {
        throw "No redmine.Request passed in.";
    }
    this.request = request;
};

/**
 * loa list of users
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Users#Users
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Users.prototype.all = function(callback) {
    callback = callback || redmine.callback;
    this.request.get("/users.json", callback);
};

/**
 * Returns the user details.
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Users#GET-2
 * @throws {Error} If no user id provided
 * @param {(number|string)} id User id
 * @param {string=} include A coma separated list of associations to include in the response
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Users.prototype.get = function(id, include, callback) {
    if (!id) {
        throw new Error("No User ID passed.");
    }
    //check for include
    if (arguments.length < 3 && typeof include == "function") {
        callback = include;
        include = false;
    }
    include = include || false;
    if (include) {
        include = "?include="+include;
    } else {
        include = "";
    }
    callback = callback || redmine.callback;
    this.request.get("/users/"+id+".json"+include, callback);
};

/**
 * Retrieving the user whose credentials are used to access the API
 * 
 * @param {string=} include A coma separated list of associations to include in the response
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Users.prototype.me = function(include, callback) {
    //check for include
    if (arguments.length < 2 && typeof include == "function") {
        callback = include;
        include = false;
    }
    include = include || false;
    if (include) {
        include = "?include="+include;
    } else {
        include = "";
    }
    callback = callback || redmine.callback;
    this.request.get("/users/current.json"+include, callback);
};

/**
 * Creates a user.
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Users#POST
 * @throws {Error} if no user details provided
 * @param {Object} user User details
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Users.prototype.create = function(user, callback) {
    if (!user || arguments.length < 2) {
        throw new Error("No user details provided.");
    }
    callback = callback || redmine.callback;
    var data = {
        'user': user
    };
    this.request.post("/users.json", JSON.stringify(data), callback);
};

/**
 * Updates a user.
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Users#PUT
 * @throws {Error} if no user details provided
 * @param {(number|string)} id User id
 * @param {Object} user User details
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Users.prototype.update = function(id, user, callback) {
    if (!id) {
        throw new Error("No user id provided.");
    }
    if (!user || typeof user != "object") {
        throw new Error("No user details provided.");
    }
    if (arguments.length < 3) {
        throw new Error("No user id or details provided.");
    }
    callback = callback || redmine.callback;
    var data = {
        'user': user
    };
    this.request.put("/users/"+id+".json", data, callback);
};

/**
 * Deletes a user.
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Users#DELETE
 * @throws {Error} if no user id provided
 * @param {(number|string)} id User id
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Users.prototype.del = function(id, callback) {
    if (!id || arguments.length < 2) {
        throw new Error("No user id provided.");
    }
    callback = callback || redmine.callback;
    this.request.del("/users/"+id+".json", callback);
};