'use strict';

/**
 * Base redmine API namespace
 * 
 * @namespace {redmine}
 */
var redmine = window.redmine = {};

/**
 * Default configuration for Redmine Request
 * 
 * @protected
 * @type {Object}
 */
redmine.Config = {
    host: "",
    useHttpAuth: false,
    httpUser: "",
    httpPassword: "",
    chiliProject: false,
    apiAccessKey: ""
};

/**
 * Default callback function for API
 * 
 * @param {?Object} error Error details
 * @param {?Object} data Data recieved from Redmine REST API
 */
redmine.callback = function(error, data) {};

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * 
 * @protected
 * @param {Object} obj1
 * @param {Object} obj2
 * @returns {Object}
 */
redmine.merge = function(obj1, obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
};