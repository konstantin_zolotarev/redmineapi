'use strict';

/**
 * Create API object
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @param {redmine.Config} config
 * @returns {redmine.Api}
 */
redmine.Api = function(config) {
    //request create
    this.request = new redmine.Request(config);
    this.projects = new redmine.Projects(this.request);
    this.issues = new redmine.Issues(this.request);
    this.news = new redmine.News(this.request);
    this.users = new redmine.Users(this.request);
    this.time_entry = new redmine.TimeEntries(this.request);
};

/**
 * Upload new file to redmine
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_api#Attaching-files
 * @since 1.4
 * @param {?} file File data
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Api.prototype.upload = function(file, callback) {
    if (arguments.length < 2 || !file) {
        throw new Error("Please provide all parameters");
    }
    callback = callback || redmine.callback;
    this.request.upload("/uploads.json", file, callback);
};

