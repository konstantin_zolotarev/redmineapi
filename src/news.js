/**
 * Base News API methods
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @constructor
 * @struct
 * @since 1.1
 * @throws {Error} No redmine.Request passed in.
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_News
 * @param {redmine.Request} request
 * @returns {redmine.News}
 */
redmine.News = function(request) {
    if (!request || !request instanceof redmine.Request) {
        throw new Error("No redmine.Request passed in.");
    }
    this.request = request;
};

/**
 * Load news or news from project
 * 
 * @param {(number|string)=} projectId project Id or identifier to load news from 
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.News.prototype.all = function(projectId, callback) {
    if (arguments.length == 1 && typeof projectId == "function") {
        callback = projectId;
        projectId = false;
    }
    var url = "";
    if (!projectId) {
        url = "/news.json";
    } else {
        url = "/projects/"+projectId+"/news.json";
    }
    callback = callback || redmine.callback;
    this.request.get(url, callback);
};