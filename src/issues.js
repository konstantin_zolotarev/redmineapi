/**
 * Base Issues API methods
 * 
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 * @constructor
 * @struct
 * @since 1.0
 * @throws {Error} No redmine.Request passed in.
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues
 * @param {redmine.Request} request
 * @returns {redmine.Issues}
 */
redmine.Issues = function(request) {
    if (!request || !request instanceof redmine.Request) {
        throw new Error("No redmine.Request passed in.");
    }
    this.request = request;
};

/**
 * Load list of issues from Redmine
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues#Listing-issues
 * @param {?string} filters list of additional filters. For ex: project_id=2&tracker_id=1
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.all = function(filters, callback) {
    if (arguments.length < 2 && typeof filters == "function") {
        callback = filters;
        filters = "";
    }
    filters = filters || "";
    callback = callback || redmine.callback;
    this.request.get("/issues.json?"+filters, callback);
};

/**
 * Get issue details
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues#Showing-an-issue
 * @throws {Error} if no issue ID passed
 * @param {(number|string)} id Issue id
 * @param {?string} include fetch associated data (optional, use comma to fetch multiple associations).
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.get = function(id, include, callback) {
    if (!id || arguments.length < 2) {
        throw new Error("No issue ID passed.");
    }
    //check for include
    if (arguments.length < 3 && typeof include == "function") {
        callback = include;
        include = false;
    }
    callback = callback || redmine.callback;
    include = include || false;
    if (include) {
        include = "?include="+include;
    } else {
        include = "";
    }
    this.request.get("/issues/"+id+".json"+include, callback);
};

/**
 * Add new comment to issue
 * 
 * @param {(number|string)} id Issue ID
 * @param {string} comment
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.comment = function(id, comment, callback) {
    if (arguments.length < 3 || !id || !comment) {
        throw new Error("Please provide all parameters");
    }
    callback = callback || redmine.callback;
    var data = {
        'issue': {
            'notes': comment
        }
    };
    this.request.put("/issues/"+id+".json", data, callback);
};

/**
 * Create new issue 
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues#Creating-an-issue
 * @throws {Error} if no issue provided
 * @param {Object} issue issue details
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.create = function(issue, callback) {
    if (!issue) {
        throw new Error("No issue details provided");
    }
    callback = callback || redmine.callback;
    var data = {
        'issue': issue
    };
    this.request.post("/issues.json", JSON.stringify(data), callback);
};

/**
 * Update issue
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues#Updating-an-issue
 * @throws {Error} if no ID or issue Details provided
 * @param {(number|string)} id issue ID
 * @param {Object} issue issue details
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.update = function(id, issue, callback) {
    if (!id) {
        throw new Error("No issue ID provided.");
    }
    if (!issue) {
        throw new Error("No issue details provided");
    }
    callback = callback || redmine.callback;
    var data = {
        'issue': issue
    };
    this.request.put("/issues/"+id+".json", JSON.stringify(data), callback);
};

/**
 * Removing issue
 * 
 * @param {(number|string)} id Issue ID
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.del = function(id, callback) {
    if (!id) {
        throw new Error("No issue ID provided.");
    }
    callback = callback || redmine.callback;
    this.request.del("/issues/"+id+".json", callback);
};


/**
 * Add new Watcher to issue
 * 
 * @since 2.3.0
 * @throws {Error} if no issue ID or User ID provided
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues#Adding-a-watcher
 * @param {(nubmer|string)} id Issue ID
 * @param {(nubmer|string)} userId User ID
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.addWatcher = function(id, userId, callback) {
    if (!id) {
        throw new Error("No issue ID provided.");
    }
    if (!userId) {
        throw new Error("No user ID provided");
    }
    callback = callback || redmine.callback;
    var data = {
        'user_id': userId
    };
    this.request.post("/issues/"+id+"/watchers.json", JSON.stringify(data), callback);
};

/**
 * Remove Watcher from issue
 * 
 * @since 2.3.0
 * @throws {Error} if no issue ID or User ID provided
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Issues#Removing-a-watcher
 * @param {(nubmer|string)} id Issue ID
 * @param {(nubmer|string)} userId User ID
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.removeWatcher = function(id, userId, callback) {
    if (!id) {
        throw new Error("No issue ID provided.");
    }
    if (!userId) {
        throw new Error("No user ID provided");
    }
    callback = callback || redmine.callback;
    this.request.del("/issues/"+id+"/watchers/"+userId+".json", callback);
};

/**
 * Get issue statuses list
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_IssueStatuses
 * @since 1.3
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.statuses = function(callback) {
    callback = callback || redmine.callback;
    this.request.get("/issue_statuses.json", callback);
};

/**
 * Get issue statuses list
 * 
 * @link http://www.redmine.org/projects/redmine/wiki/Rest_Enumerations#enumerationsissue_prioritiesformat
 * @since 2.2
 * @param {function(?Object, ?Object)=} callback Callback for request
 */
redmine.Issues.prototype.priorities = function(callback) {
    callback = callback || redmine.callback;
    this.request.get("/enumerations/issue_priorities.json", callback);
};