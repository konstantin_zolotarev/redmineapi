'use strict';

var _ = require('lodash');
var Request = require('./Request');

module.exports = Client;

Client.Config = {
    /**
     * Redmine host
     *
     * @type string
     */
    host: "",

    /**
     * Will redmine require HTTP Auth ?
     *
     * @type boolean
     */
    useHttpAuth: false,

    /**
     * HTTP Auth user
     *
     * @type string
     */
    httpUser: "",

    /**
     * HTTP Auth password
     *
     * @type string
     */
    httpPassword: "",

    /**
     * Is this is Chili project
     *
     * @type boolean
     */
    isChiliProject: false,

    /**
     * Redmine API key for auth
     *
     * @type string
     */
    apiAccessKey: ""
};

/**
 * Base Redmine REST Client
 *
 * @param {object} options
 * @constructor
 */
function Client(options) {
    //inside config
    this.config = _.merge(Client.Config, options);
    this.request = new Request(this.config);
};

/**
 * Will ping Redmine server
 *
 * @param {Function=} [cb]
 */
Client.ping = function(cb) {
    cb = cb || function() {};

};