'use strict';

var request = require('request');
var _ = require('lodash');
var path = require('path');

module.exports = Request;

/**
 * Will create a base requester for Redmine
 *
 * @param {Object} options
 * @constructor
 */
function Request(options) {
    this.options = options;
};

/**
 * Will generate and return list of options for request
 *
 * @param {string} path URL path on Redmine server
 * @private
 */
Request.prototype._getOptions = function(path) {
    var opts = {
        url: path.join(this.options.host, path),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'X-Redmine-API-Key': this.options.apiAccessKey
        }
    };
    //check ChiliProject
    if (this.options.isChiliProject) {
        opts.headers['X-ChiliProject-API-Key'] = this.options.apiAccessKey;
    }
    //check http auth
    if (this.options.useHttpAuth) {
        opts.auth = {
            user: this.options.httpUser,
            pass: this.options.httpPassword
        };
    }
    return opts;
};

/**
 * Will perform GET request to given URL part
 *
 * @param {string} url
 * @param {Function=} [cb]
 */
Request.prototype.get = function(url, cb) {
    cb = cb || function() {};
    var opts = this._getOptions(url);
    _.merge(opts, {
        method: 'GET'
    });
    request(opts, cb);
};

/**
 * Will perform POST request to Redmine
 *
 * @param {string} url
 * @param {Object|string=} [data]
 * @param {Function=} [cb]
 */
Request.prototype.post = function(url, data, cb) {
    cb = cb || function() {};
    if (_.isFunction(data)) {
        cb = data;
        data = {};
    }
    var opts = this._getOptions(url);
    _.merge(opts, {
        method: 'POST'
    });
    if (_.isPlainObject(data)) {
        opts.json = data;
    }
    if (_.isString(data)) {
        opts.data = data;
    }
    request(opts, cb);
};

/**
 * Will perform PUT request to Redmine
 *
 * @param {string} url
 * @param {Object|string=} [data]
 * @param {Function=} [cb]
 */
Request.prototype.put = function(url, data, cb) {
    cb = cb || function() {};
    if (_.isFunction(data)) {
        cb = data;
        data = {};
    }
    var opts = this._getOptions(url);
    _.merge(opts, {
        method: 'PUT'
    });
    if (_.isPlainObject(data)) {
        opts.json = data;
    }
    if (_.isString(data)) {
        opts.data = data;
    }
    request(opts, cb);
};

/**
 * Will perform DELETE request to Redmine
 *
 * @param {string} url
 * @param {Function=} [cb]
 */
Request.prototype.del = function(url, cb) {
    cb = cb || function() {};
    var opts = this._getOptions(url);
    _.merge(opts, {
        method: 'DELETE'
    });
    request(opts, cb);
};

Request.prototype.upload = function(url, data, cb) {};